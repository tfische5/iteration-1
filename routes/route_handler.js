/**
 * Copyright Digital Engagement Xperience 2014
 * Created by Andrew
 *
 */
'use strict';

// third-party modules
var _ = require('underscore');
// internal modules
var logger = require('../lib/util/logger');
var http = require('follow-redirects').http;

// Make the request
// client.userInfo(function (err, data) {
//     // ...
// });

/**
 * Add routes for a third-party channel handler to an Express application.
 *
 * @param app express application
 * @param {AbstractChannelHandler} handler handler for processing third-party channel requests
 */
module.exports = function (app, handler) {

    if (!handler) {
        throw new Error('Service not found!');
    }

    app.get('/authorize', function (req, res) {
        logger.debug("authorize:", req.body);
        var authorized = handler.authorize(function(err, url) {
            if (err) {
                next(err);
            } else {
                res.status(200).redirect(url);
            }
        });
    });

    app.post('/accept', function (req, res) {
        logger.debug("accept:", req.body);
        var accepted = handler.accept(req.body, function(err, value) {
            logger.debug("accepted:", value);

            if (err) {
                next(err);
            } else {
                res.status(200).send({ 'accepted' : value });
            }
        });
    });

    app.get('/callback', function (req, res) {
        var callback = handler.callback(req, function(err, result) {
            if (err) {
                next(err);
            } else {
                logger.debug("delivered:", result);
                res.status(200).send({ 'accepted' : result });
            }
        });
    });

    app.post('/deliver', function(req, res, next) {
        handler.deliver(req.body.params, req.body.playlist, function(err, id, host) {
            if (err) {
                next(err);
            } else {
                logger.debug("delivered:", id + " " + host);
                res.status(200).send({ 'id' : id, 'host' : host });
            }
        });
    });

    app.post('/feedback', function(req, res, next) {
        handler.getFeedback(req.body, function(err, feedback) {
            if (err) {
                next(err);
            } else {
                logger.debug("feedback:", feedback);
                res.status(200).send({ 'notes returned' : feedback });
            }
        });
    });

    app.post('/remove', function(req, res, next) {
        logger.debug("remove: ", req.body);
        handler.remove(req.body, function(err, result) {
            if (err) {
                next(err);
            } else {
                res.status(200).send({ message: result} );
            }
        });
    });

    return app;
};