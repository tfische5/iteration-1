/**
 * Copyright Digital Engagement Xperience 2014
 * Created by Andrew
 *
 */

 /**
  *	  Everytime someone logs in, add a new record to this table
  *
  *   Blogs(bid, url)
  *	  PK: bid
  *
  *	  Everytime we add/remove a post, add a reference to it so we can grab data from JSON
  *
  *   Posts(pid, url, bid)
  *	  PK: pid
  *   FK: bid references Blogs(bid)
  *
  *  CREATE TABLE Blogs(
  *	 	bid SERIAL PRIMARY KEY NOT NULL,
  *	 	url TEXT NOT NULL
  *  );
  *
    CREATE TABLE Posts(
  		pid INT PRIMARY KEY NOT NULL,
  		url TEXT NOT NULL,
  		bid INT NOT NULL REFERENCES Blogs(bid)
    );
  *
  *	 Following is the info for the Knowledge Broker (Already made it, just in case we break it)
  *  jdbc:postgresql://ec2-54-235-99-46.compute-1.amazonaws.com:5432/d5ood78c78ag8
  *  hgluaiyzaztxvj
  *  9jmWrFw1dkXscOvCCdWSElG-n9
  *
  */


  /* --------------------------------------------------------------------------------------- */

  var https = require('https'),
  _ = require('underscore'),
  formUrlEncoded = require('form-urlencoded');

  var dexToken;

  function OAuth2Authentication() {
  	this.protocol = https;
   this.host = 'sso.dexit.co'; // dex thing
   this.port = 443; // 443
   this.path = '/openam/oauth2/access_token?realm=adet'; 
   //this.clientId = config.oauth2.clientId;
   //this.clientSecret = config.oauth2.clientSecret;
   //this.realm = config.auth.realm;
}

OAuth2Authentication.prototype.authenticate = function () {
	var realm = 'adet';

	var body = formUrlEncoded.encode({
		grant_type: 'password',
		username: 'adam_gencarelli@hotmail.com',
		password: 'babybear'
	});

	var options = {
		host: this.host,
		port: this.port,
		path: this.path,
		method: 'POST',
		headers: {
			"Content-Type": 'application/x-www-form-urlencoded',
			"Authorization": 'Basic ZHgtc2VydmljZToxMjMtNDU2LTc4OQ=='
		},
		msgbody: body
	};
   //Create the request
   var req = this.protocol.request(options, function (res) {
   	res.setEncoding("utf8"); 
   	res.on("data", function(result) { 
   		console.log(result);
   		dexToken = JSON.parse(result);
   		console.log(dexToken.access_token);
   	});
   }); 
   //End the request
   req.on("error", function (e) {
   	utils.handleRequestError(e, callback);
   });
   req.end(options.msgbody);
};

OAuth2Authentication.prototype.checkTokenValidity = function (token, callback) {
	var self = this;
	if (!dexToken) {
		return callback(null, false);
	}
	var options = {
		host: this.host,
		port: this.port,
		path: this.path + '/tokeninfo?access_token=' + dexToken,
		method: 'GET'
	};
   //Create the request
   var req = this.protocol.request(options, function (res) {
   	self._handleResponse(res, function (err, result) {
   		if (err) {
   			callback(err);
   		} else if (result && result.expires_in) {
   			console.log(result);
   			callback(null, true);
   		} else {
   			callback(null, false);
   		}
   	}, true);
   });
   //End the request
   req.on("error", function (e) {
   	self._handleError((e, callback));
   });
   req.end(options.msgbody);
};


/**
 * Handle errors
 * @param res
 * @param callback
 * @private
 */
 OAuth2Authentication.prototype._handleError = function(res, callback) {

 };

/**
 * Utility to handle response codes
 * @param res
 * @param callback
 * @private
 */
 OAuth2Authentication.prototype._handleResponse= function(res, callback) {

   //Variable to deal with the response
   var str = '';
   //Set the encoding of the response to UTF8
   res.setEncoding('utf8');

   //chunked data
   res.on('data', function (chunk) {
       //Append the chunk of data to the string
       str += chunk;
   });

   //End of the response - Response Received
   res.on('end', function () {

   	var error, data;

   	switch (res.statusCode) {
   		case 200:
   		case 201:
   		if(str) {
   			try {
   				data = JSON.parse(str);
   			} catch(e){
   				data = str;
   			}
   		}
   		break;
   		case 400:
   		error = new Error("Bad request");
   		break;

   		case 401:
   		error = new Error("Unauthorized");
   		break;

   		case 403:
   		error = new Error("Forbidden");
   		break;
   		case 404:
   		error = new Error('not found');
   		break;

   		default:
   		error = new Error("unhandled response");
   		break;
   	}

   	if(error){
   		error.status = res.statusCode;
   	}
   	callback( error, data );
   });
};


OAuth2Authentication.prototype.logout = function (token, callback) {
	console.log("OAuth2 logout not supported");
	callback(null, undefined);
};

module.exports = OAuth2Authentication;

var auth = new OAuth2Authentication (); 
auth.authenticate();

/* --------------------------------------------------------------------------------------- */

var HttpCodeError = require('./util/http-code-error');
var logger = require('./util/logger');
var apiKey = "cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ";
var http = require("http");
var OAuth = require('oauth').OAuth;
var tumblr = require('tumblr.js');
/*var client = tumblr.createClient({
   consumer_key: 'cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ',
   consumer_secret: 'NRCwttN4bJhmtLxoj8kqk5Buc4bXQJ1cMl9IOhid8aDOxvllTA',
   token: 'iUyHWq3MbtFU0uIXn5uFWcTogoUobIKsYdJg8tIKbINu6l8VcJ',
   token_secret: 'Y2ZrHPq3c9sFVqyXMn3zZEkvQ4wi8FFJ4Uqza3haiW7AwfbzsW'
});*/
var consumer = new OAuth("https://www.tumblr.com/oauth/request_token",
	"https://www.tumblr.com/oauth/access_token",
	"cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ",
	"NRCwttN4bJhmtLxoj8kqk5Buc4bXQJ1cMl9IOhid8aDOxvllTA",
	"1.0A",
	"http://se3350-iteration-1.herokuapp.com/myhandler/callback",
  //"http://localhost:5050/myhandler/callback",
	"HMAC-SHA1");

var pg = require('pg');
var oauthRequestToken;
var oauthTokenSecret;
var client;


/**
 * Constructor for abstract channel handlers.
 * This class should be extended as necessary by concrete handler implementations.
 * @constructor
 */
 function AbstractChannelHandler() {
 }

 function buildLogMsg(action, msg) {
 	return "resource: abstract channel handler, action: " + action + ", " + msg;
 }

 AbstractChannelHandler.prototype.authorize = function(callback) {
  consumer.getOAuthRequestToken(function(error, oauthToken, oauthTokenSecret) {
    if (error) {
      callback(new HttpCodeError(500, "Error getting OAuth request token: " + error));
    } else {
      oauthRequestToken = oauthToken,
      oauthRequestTokenSecret = oauthTokenSecret;
      callback(undefined, "http://www.tumblr.com/oauth/authorize?oauth_token=" + oauthRequestToken);
    }
  });
 };

/**
 * Determine whether a channel instance is compatible with the handler.
 *
 * @param {Object} channel channel instance to test
 * @return {Boolean} true if accepted, false otherwise
 */
 AbstractChannelHandler.prototype.accept = function (channel, callback) {
  console.log(channel.url);
  var host = channel.url.replace(/^.*?:\/\//, "");
  host = host.replace(/\/$/, "");
 	var url = "/v2/blog/" + host + "/info?api_key=" + apiKey;
 	var message = "not supported by this channel handler";

 	console.log(host);

 	var options = {
 		host: "api.tumblr.com",
 		post: 80,
 		path: url,
 		method: "GET"
 	};

 	var request = http.request(options, function(res) {
 		res.setEncoding("utf8");
 		res.on("data", function(json) {
 			var data = JSON.parse(json);

 			if (data.meta.status == 200) {
 				message = "This blog is acceptable";
        callback(undefined, true);
 			} else {
 				callback(undefined, false);
 			} 

 			logger.info(buildLogMsg("accept", "msg: " + message));
 		});
	});

	request.end();
};



/**
 * Deliver a content playlist to a channel instance.
 * Result should be an array of objects corresponding to the posted SC instances.
 *
 * Result objects must at a minimum consist of { scObjectId: '...' } and should be
 * extended with any other data necessary to uniquely reference the deployed content
 * (e.g. post ID).
 *
 * @param {Object} params delivery parameters
 * @param {Object} playlist content playlist
 * @param {Function} callback invoked as callback([error], [result]) when finished
 */

 AbstractChannelHandler.prototype.callback = function (req, callback) {
    //console.log(req);
    consumer.getOAuthAccessToken(oauthRequestToken, oauthRequestTokenSecret, req.query.oauth_verifier, function(error, _oauthAccessToken, _oauthAccessTokenSecret) { 
    	if(error) {
    		callback(new HttpCodeError(500, "Error getting OAuth request token: " + error));
    	} else {
    		client = tumblr.createClient({
    			consumer_key: 'cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ',
    			consumer_secret: 'NRCwttN4bJhmtLxoj8kqk5Buc4bXQJ1cMl9IOhid8aDOxvllTA',
    			token: _oauthAccessToken,
    			token_secret: _oauthAccessTokenSecret,
    		});

        client.userInfo(function(err, data) {
          console.log(data.name);
          //var host = data.user.url.replace(/^.*?:\/\//, "");
          //host = host.replace(/\/$/, "");
          var selectQuery = "SELECT * FROM Blogs WHERE url='" + data.user.name + ".tumblr.com';";

          var selectOptions = {
            host: 'developer.kb.dexit.co',
            port: 80,
            path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(selectQuery),
            method: 'GET',
            headers: {
              "Authorization": 'Bearer ' + dexToken.access_token
            }
          };

          var selectRequest = http.request(selectOptions, function(res) {
            res.setEncoding("utf8");
            res.on("data", function(json) {
              var selectData = JSON.parse(json);
              if (selectData.result.rows.length > 0) return;

              var insertQuery = 'INSERT INTO Blogs (url, token, secret) VALUES (\'' + 
                                  data.user.name + '.tumblr.com\', \'' +
                                  _oauthAccessToken + '\', \'' + 
                                  _oauthAccessTokenSecret + '\');';
              console.log(insertQuery);

              var options = {
                host: 'developer.kb.dexit.co',
                port: 80,
                path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(insertQuery),
                method: 'GET',
                headers: {
                  "Authorization": 'Bearer ' + dexToken.access_token
                }
              };

              var insertRequest = http.request(options, function(res) {
                res.setEncoding("utf8");
                res.on("data", function(chunk) {
                  console.log("\nNew blog inserted");
                });
              });

              insertRequest.end();
            });
          });

          selectRequest.end();
        });

    		callback(undefined, true);
    	}
    });
}

AbstractChannelHandler.prototype.deliver = function (params, playlist, callback) {
  //logger.info(buildLogMsg("deliver", "msg: not supported by this channel handler"));
  //callback();
  var text = playlist[0].multimedia.text;
  var image = playlist[0].multimedia.image;
  var video = playlist[0].multimedia.video;
  var audio = playlist[0].multimedia.audio;
  var link = playlist[0].multimedia.link;

  var selectQuery = "SELECT * FROM Blogs WHERE url='" + params.channel.url + "';";
  var host = params.channel.url.replace(/^.*?:\/\//, "");
  host = host.replace(/\/$/, "");

  var selectQuery = "SELECT * FROM Blogs WHERE url='" + host+ "';";

  var selectOptions = {
    host: 'developer.kb.dexit.co',
    port: 80,
    path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(selectQuery),
    method: 'GET',
    headers: {
      "Authorization": 'Bearer ' + dexToken.access_token
    }
  };

  var selectRequest = http.request(selectOptions, function(res) {
    res.setEncoding("utf8");
    res.on("data", function(json) {
      var selectData = JSON.parse(json);

      console.log(selectData.result);

      client = tumblr.createClient({
        consumer_key: 'cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ',
        consumer_secret: 'NRCwttN4bJhmtLxoj8kqk5Buc4bXQJ1cMl9IOhid8aDOxvllTA',
        token: selectData.result.rows[0][2],
        token_secret: selectData.result.rows[0][3],
      });
      
      if (!client) {
        callback(new HttpCodeError(501, 'No client initiated'));
        return;
      }

      client.userInfo(function (err, data) {
        var postOptions = {
          caption  : text[0].property.content,
          source   : image[0].property.location
        };

        client.photo(data.user.name, postOptions, function(err, res) {
          if (err) throw err;

          console.log(res);

          addToDatabase(data.user.name + '.tumblr.com', res.id, function(id, host) {
            callback(undefined, id, host);
          });
        });
      });   
    });
  });

  selectRequest.end();
};

function addToDatabase(host, postID, callback) {
  var selectQuery = "SELECT bid FROM Blogs WHERE url='" + host + "';";

  var selectOptions = {
    host: 'developer.kb.dexit.co',
    port: 80,
    path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(selectQuery),
    method: 'GET',
    headers: {
      "Authorization": 'Bearer ' + dexToken.access_token
    }
  };

  var selectRequest = http.request(selectOptions, function(res) {
    res.setEncoding("utf8");
    res.on("data", function(json) {
      var selectData = JSON.parse(json);

      console.log(selectData.result);

      var insertQuery = "INSERT INTO Posts(pid, bid) VALUES (" + postID + ", " + selectData.result.rows[0][0] + ");";
      var options = {
        host: 'developer.kb.dexit.co',
        port: 80,
        path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(insertQuery),
        method: 'GET',
        headers: {
          "Authorization": 'Bearer ' + dexToken.access_token
        }
      };

      var insertRequest = http.request(options, function(res) {
        res.setEncoding("utf8");
        res.on("data", function(json) {
          console.log(json);
          callback(postID, host);
        });
      });

      insertRequest.end();
    });
  });

  selectRequest.end();
}

/**
 * Get feedback (e.g. replies, comments) from previously delivered content.
 * Result should be an array of objects which use the format provided by
 * translateFeedback().
 *
 * @param {Object} params content parameters
 * @param {Function} callback invoked as callback([error], [result]) when finished
 */
 AbstractChannelHandler.prototype.getFeedback = function (params, callback) {
 	// logger.info(buildLogMsg("getFeedback", "msg: not supported by this channel handler"));
 	// callback(new HttpCodeError(501, 'getFeedback not implemented'));

    if (!client) {
      callback(new HttpCodeError(501, 'No client initiated'));
      return;
    }

    var url = "/v2/blog/" + params.blog + "/posts?api_key=" + apiKey;
    var message = "not supported by this channel handler";

    var options = {
      host: "api.tumblr.com",
      post: 80,
      path: url,
      method: "POST"
    };

    var request = http.request(options, function(res) {
      res.setEncoding("utf8");
      var data = "";

      res.on("data", function(chunk) {
        data += chunk;
      });

      res.on("end", function() {
        var info = JSON.parse(data);
        var posts = info.response.posts;

        //var dataStore = channel.dataStore;
        var noteQuery = "SELECT * FROM Posts;";

        var noteOptions = {
          host: 'developer.kb.dexit.co',
          port: 80,
          path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(noteQuery),
          method: 'GET',
          headers: {
            "Authorization": 'Bearer ' + dexToken.access_token
          }
        };

        var noteRequest = http.request(noteOptions, function(response) {
          response.setEncoding('utf8');
          response.on("data", function(data) {
            var jsonData = JSON.parse(data);
            var postsFromDB = jsonData.result.rows; //all posts from db
            var notes = { }; //matched notes

            postsFromDB.forEach(function (dbPost) { //loop through db posts
              posts.forEach(function (post) { //loop through tumblr posts
                if (post.id == dbPost[0]){
                  notes[post.id] = post.note_count;
                }
              });
            });

            callback(undefined, notes);
          });
        });
        noteRequest.end(); //end db request
      });
      
    });
    request.end(); //end dex request
 };

/**
 * Remove previously delivered content from the channel instance.
 *
 * The SC instance objects passed in will match those which were provided in the
 * response to the deliver() call.
 *
 * @param {Object[]} scInstances SC instances to be deleted
 * @param {Function} callback invoked as callback([error]) when finished
 */
 AbstractChannelHandler.prototype.remove = function (scInstances, callback) {
  var host = scInstances[0].host;
  var id = scInstances[0].id;
  console.log("-------> " + host + " " + id);

  var selectQuery = "SELECT * FROM Blogs WHERE url='" + host + "';";
  //console.log(params.channel);

  var selectOptions = {
    host: 'developer.kb.dexit.co',
    port: 80,
    path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(selectQuery),
    method: 'GET',
    headers: {
      "Authorization": 'Bearer ' + dexToken.access_token
    }
  };

  var selectRequest = http.request(selectOptions, function(res) {
    res.setEncoding("utf8");
    res.on("data", function(json) {
      var selectData = JSON.parse(json);

      console.log(selectData.result);
      console.log(selectData.result.rows[0][2] + " " + selectData.result.rows[0][3]);

      client = tumblr.createClient({
        consumer_key: 'cKdwijf6JpYfdwoRYfAgzjw1Bo6Ecnm3nNvMvgUuWh69Hj3nHQ',
        consumer_secret: 'NRCwttN4bJhmtLxoj8kqk5Buc4bXQJ1cMl9IOhid8aDOxvllTA',
        token: selectData.result.rows[0][2],
        token_secret: selectData.result.rows[0][3],
      });

      if (!client) {
        callback(new HttpCodeError(501, 'No client initiated'));
        return;
      }

      client.userInfo(function (err, data) { 
        //console.log(data.user);
        var deleteQuery = "DELETE FROM Posts WHERE pid = " + id + ";"; 

        var deleteDBOptions = { 
          host: 'developer.kb.dexit.co',  
          port: 80, 
          path: '/access/stores/ice4e/query/?query=' + encodeURIComponent(deleteQuery), 
          method: 'GET',
          headers: {
            "Authorization": 'Bearer ' + dexToken.access_token
          }
        }; 

        client.deletePost(data.user.name, id, function(err, res) { 
          if (err) { 
            throw err; 
          } 

          var deleteDBRequest = http.request( deleteDBOptions, function(res) {
            res.setEncoding('utf8'); 
            res.on("data", function(chunk) { 
              console.log(chunk);
              console.log("deleting a post from the database"); 
              callback(undefined, "Successful delete");
            });
          });  // end deleteDBRequest

          deleteDBRequest.end(); 
        }); // end client.text
      }); // end client.userInfo
    });
  });

  selectRequest.end();

 };


 module.exports = AbstractChannelHandler;